﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ProductsWebApp.Database;
using ProductsWebApp.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ProductsWebApp.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ProductsController : ControllerBase
    {
        private readonly ILogger<ProductsController> _logger;

    private ApplicationContext db;

    public ProductsController(ILogger<ProductsController> logger, ApplicationContext context)
    {
        _logger = logger;
        db = context;
    }

        [HttpGet]
        public ProductsResponse Get(int page, int pageSize)
        {
            var products = db.Products.Skip((page - 1) * pageSize).Take(pageSize);
            var count = db.Products.Count();
            return (new ProductsResponse { Products = products, Count = count, Success= true });
        }
    }
}
