﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ProductsWebApp.Database;
using ProductsWebApp.Models;

namespace ProductsWebApp.Controllers
{
    [ApiController]
    [Authorize]
    [Route("[controller]")]
    public class AddProductController : ControllerBase
    {

        private readonly ILogger<AddProductController> _logger;

        private ApplicationContext db;
        public AddProductController(ILogger<AddProductController> logger, ApplicationContext context)
        {
            _logger = logger;
            db = context;
        }

        [HttpPost]
        public async Task<Message> Post(AddProductRequest request)
        {
            if (!request.isValid()) {
                return new Message { Error = "Не заполнены обязательные поля", Success = false };
            }
            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    db.Products.Add(request.toSet());
                    await db.SaveChangesAsync();
                    await transaction.CommitAsync();
                }
                catch (Exception ex)
                {
                   await transaction.RollbackAsync();
                   return new Message { Error = ex.Message, Success = false };
                }
            }

            return new Message {Success = true };
        }
    }
}
