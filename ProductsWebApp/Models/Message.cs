﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProductsWebApp.Models
{
    public class Message
    {
        public bool Success { get; set; }
        public string Error { get; set; }
    }
}
