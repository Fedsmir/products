﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProductsWebApp.Models
{
    public class ProductsResponse : Message
    {
        public IEnumerable<Product> Products { get; set; }
        public int Count { get; set; }
    }
}
