﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProductsWebApp.Models
{
    public class AddProductRequest
    {
        public string Label { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }

        public bool isValid()
        {
            return !string.IsNullOrEmpty(Label) && !string.IsNullOrEmpty(Description) && Price > 0;
        }

        public Product toSet() =>
            new Product
            {
                Description = Description,
                Label = Label,
                Price = Price
            };
    }
}
