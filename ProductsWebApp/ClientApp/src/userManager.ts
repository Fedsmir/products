import { createUserManager } from 'redux-oidc';
import { UserManagerSettings } from 'oidc-client';

const userManagerConfig: UserManagerSettings = {
  client_id: 'api1',
    redirect_uri: 'https://localhost:5003/callback',
  response_type: 'token id_token',
  scope: "openid profile api1",
  authority: 'https://localhost:5001',
  silent_redirect_uri: 'http://localhost:5100/silentRenew.html',
  automaticSilentRenew: false,
  filterProtocolClaims: true,
  loadUserInfo: true,
  monitorSession: true
};

const userManager = createUserManager(userManagerConfig);

export default userManager;