import * as React from "react";
import { connect } from "react-redux";
import { RouteComponentProps } from "react-router";
import { ApplicationState } from "../store";
import * as AddProductStore from "../store/AddProduct";
import { Form, Input, Button, Checkbox, InputNumber, Modal } from "antd";
import TextArea from "antd/lib/input/TextArea";
import { PriceInput } from "./PriceInput";
import userManager from "../userManager";

type AddProductProps = AddProductStore.AddProductState &
  ApplicationState &
  typeof AddProductStore.actionCreators &
  RouteComponentProps<{}>;

const layout = {
  labelCol: { span: 8 },
  wrapperCol: { span: 16 },
};
const tailLayout = {
  wrapperCol: { offset: 8, span: 16 },
};

const login = () => {
  userManager.signinRedirect({
    data: { path: '/add' },
  });
};

class AddProduct extends React.PureComponent<AddProductProps> {

  onFinish = (values: any) => {
    console.log("Success:", values);
    this.props.addProduct({
      description: values.description,
      label: values.label,
      price: values.price.number,
    });
  };

  onFinishFailed = (errorInfo: any) => {
    console.log("Failed:", errorInfo);
  };

  checkPrice = (_rule: any, value: { number: number }) => {
    if (!!value) {
      if (value.number > 0) {
        return Promise.resolve();
      }
      return Promise.reject("Стоимость должна быть больше нуля!");
    }
    return Promise.reject("Пожалуйста, введите стоимость товара.");
  };

    public componentDidUpdate() {
    if (this.props.addProduct.addResponse) {
      debugger;
      this.showResult(this.props.addProduct.addResponse);
    }
    }

  showResult(addResponse: AddProductStore.AddResponse) {
    if (addResponse.success) {
        this.success();
    } else {
      this.error(addResponse.error);
    }
  }
  success() {
    Modal.success({
      title: "Товар добавлен успешно"
    });
  }

  error(text: string) {
    Modal.error({
      title: "Произошла ошибка",
      content: text,
    });
  }

  getUserInfo() {
    console.log(this.props.oidc.user);
    return '';
  }

  public render() {
    return (
      <React.Fragment>
        <div>{this.getUserInfo()}</div>
        {!this.props.oidc.user ?
          <div><div>Войдите, чтобы иметь возможность добавлять товары.</div><Button onClick={() => login()}>
            Вход
          </Button></div> : <Form
            {...layout}
            name="basic"
            initialValues={{ remember: true }}
            onFinish={this.onFinish}
            onFinishFailed={this.onFinishFailed}
          >
            <Button onClick={event => {
              event.preventDefault();
              userManager.removeUser();
            }}>Выход</Button>
            <Form.Item
              label="Название"
              name="label"
              rules={[
                {
                  required: true,
                  message: "Пожалуйста, введите наименование товара.",
                },
              ]}
            >
              <Input />
            </Form.Item>

            <Form.Item
              label="Описание"
              name="description"
              rules={[
                {
                  required: true,
                  message: "Пожалуйста, введите описание товара.",
                },
              ]}
            >
              <TextArea />
            </Form.Item>
            <Form.Item
              name="price"
              label="Стоимость"
              rules={[{ validator: this.checkPrice }]}
            >
              <PriceInput />
            </Form.Item>
            <Form.Item {...tailLayout}>
              <Button type="primary" htmlType="submit">
                Submit
            </Button>
            </Form.Item>
          </Form>}


      </React.Fragment>
    );
  }
}
function mapStateToProps(state: ApplicationState) {
  return {
    oidc: state.oidc,
    addProduct: state.addProduct
  };
}


export default connect(
  mapStateToProps,
  AddProductStore.actionCreators
)(AddProduct as any);
