import * as React from "react";
import { connect } from "react-redux";
import { RouteComponentProps } from "react-router";
import { ApplicationState } from "../store";
import * as ProductListStore from "../store/ProductList";
import { Form, Input, Button, Checkbox, InputNumber, Modal, List, Skeleton } from "antd";
import TextArea from "antd/lib/input/TextArea";
import { PriceInput } from "./PriceInput";
import userManager from "../userManager";

type ProductListProps = ProductListStore.ProductListState &
  ApplicationState &
  typeof ProductListStore.actionCreators &
  RouteComponentProps<{}>;

const layout = {
  labelCol: { span: 8 },
  wrapperCol: { span: 16 },
};
const tailLayout = {
  wrapperCol: { offset: 8, span: 16 },
};

const login = () => {
  userManager.signinRedirect({
    data: { path: '/add' },
  });
};

class ProductList extends React.PureComponent<ProductListProps> {

  public componentDidMount() {
    this.ensureDataFetched();
  }

  public componentDidUpdate() {
    this.ensureDataFetched();
    if (this.props.productsResponse.error) {
      this.error(this.props.productsResponse.error);
    }
  }

  private ensureDataFetched() {
    if(!this.props.isLoading || !this.props.productsResponse.success){
      this.props.requestProductList(1, 10);
    }
    
  }

  showResult(response: ProductListStore.ProductListResponse) {
    if (!response.success) {
      this.error(response.error);
    }  
  }

  error(text: string) {
    Modal.error({
      title: "Произошла ошибка",
      content: text,
    });
  }

  public render() {
    return (
      <React.Fragment>
        <List
          itemLayout="horizontal"
          dataSource={this.props.productsResponse.products}
          renderItem={item => (
              <List.Item>
                <Skeleton title={false} loading={this.props.isLoading} active>
                  <List.Item.Meta
                    title={item.label}
                    description={item.description}
                  />
                <div>{item.price} ₽</div>
                </Skeleton>
              </List.Item>
          )}
        />
      </React.Fragment>
    );
  }
}

export default connect(
  (state: ApplicationState) => state.productList, // Selects which state properties are merged into the component's props
  ProductListStore.actionCreators // Selects which action creators are merged into the component's props
)(ProductList as any);