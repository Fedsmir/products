import React from "react";
import { connect } from "react-redux";
import { CallbackComponent } from "redux-oidc";
import { push } from "connected-react-router";
import userManager from "../userManager";
import { Dispatch } from 'redux';

interface CallbackPageProps {
    dispatch: Dispatch;
    signInParams: string;
}

class CallbackPage extends React.Component<CallbackPageProps> {
  render() {
    // just redirect to '/' in both cases
    return (
      <CallbackComponent
        userManager={userManager}
        successCallback={(user) => {
          // get the user's previous location, passed during signinRedirect()
          var redirectPath = user.state.path as string;
          this.props.dispatch(push(redirectPath));
        }}
        errorCallback={error => {
          this.props.dispatch(push("/"));
          console.error(error);
        }}
        >
        <div>Redirecting...</div>
      </CallbackComponent>
    );
  }
}

export default connect()(CallbackPage);  
