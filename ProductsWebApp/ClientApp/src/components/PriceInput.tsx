import React, { useState } from 'react';
import { Form, Input, Select, Button } from 'antd';

const { Option } = Select;

interface PriceValue {
  number?: number;
}

interface PriceInputProps {
  value?: PriceValue;
  onChange?: (value: PriceValue) => void;
}

export const PriceInput: React.FC<PriceInputProps> = ({ value = { number: 0.00}, onChange }) => {
    const [number, setNumber] = useState(0.00);
    const twoPointDecimal = (number: any) => Number(number).toFixed(2);

    const triggerChange = (changedValue: PriceValue) => {
      if (onChange) {
        onChange({ number, ...value, ...changedValue });
      }
    };
  
    const onNumberChange = (e: { target: { value: any; }; }) => {
      const newNumber = parseInt(e.target.value || 0.00);
      
      if (Number.isNaN(number)) {
        return;
      }
      if (!('number' in value)) {
        setNumber(newNumber);
      }
      triggerChange({ number: newNumber });
    };
  
  
    return (
      <span>
        <Input
          type="text"
          value={value.number || number}
          onChange={onNumberChange}
          style={{ width: 200 }}
        />
      </span>
    );
  };
  