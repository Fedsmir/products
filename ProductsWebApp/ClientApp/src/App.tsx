import * as React from 'react';
import { Route } from 'react-router';
import Layout from './components/Layout';
import AddProduct from './components/AddProduct';

import './custom.css'
import CallbackPage from './components/CallbackPage';
import ProductList from './components/ProductList';

export default () => (
    <Layout>
        <Route exact path='/' component={ProductList} />
        <Route path='/callback' component={CallbackPage} />
        <Route path='/add' component={AddProduct} />
    </Layout>
);
