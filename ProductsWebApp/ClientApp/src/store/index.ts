
import * as AddProduct from './AddProduct';
import * as ProductList from './ProductList';
import { User } from 'oidc-client';
import { reducer as oidcReducer } from 'redux-oidc';

interface OidcState {
    isLoadingUser: boolean;
    user: User;
}

// The top-level state object
export interface ApplicationState {
    oidc: OidcState;
    addProduct: AddProduct.AddProductState | undefined;
    productList: ProductList.ProductListState | undefined;
}

// Whenever an action is dispatched, Redux will update each top-level application state property using
// the reducer with the matching name. It's important that the names match exactly, and that the reducer
// acts on the corresponding ApplicationState property type.
export const reducers = {
    oidc: oidcReducer,
    addProduct: AddProduct.reducer,
    productList: ProductList.reducer
};

// This type can be used as a hint on action creators so that its 'dispatch' and 'getState' params are
// correctly typed to match your store.
export interface AppThunkAction<TAction> {
    (dispatch: (action: TAction) => void, getState: () => ApplicationState): void;
}
