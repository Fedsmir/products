import { Action, Reducer } from "redux";
import { AppThunkAction } from "./";

export interface AddProductState {
  isLoading: boolean;
  addRequest?: AddRequest;
  addResponse?: AddResponse;
}

export interface AddRequest {
  label: string;
  description: string;
  price: number;
}

export interface AddResponse {
  success: boolean;
  error: string;
}

export interface RequestAddProductAction {
  type: "REQUEST_ADD_PRODUCT";
  request: AddRequest;
}

interface ReceiveAddProductAction {
  type: "RECEIVE_ADD_PRODUCT";
  response: AddResponse;
}

export type KnownAction = RequestAddProductAction | ReceiveAddProductAction;

export const actionCreators = {
  addProduct: (addRequest: AddRequest): AppThunkAction<KnownAction> => (
    dispatch,
    getState
  ) => {
    const appState = getState();
    if (appState && appState.addProduct && !appState.addProduct.isLoading) {
        if (!appState.oidc.user) {
            dispatch({
                type: "RECEIVE_ADD_PRODUCT",
                response: {error: "Ошибка авторизации", success: false},
              });
        }
      const token = appState.oidc.user.access_token;
      const headers = new Headers();
      headers.append("Accept", "application/json");
      headers.append("Content-Type", "application/json");
      headers.append("Authorization", `Bearer ${token}`);
      fetch("/addProduct", {
        method: "POST",
        headers: headers,
        body: JSON.stringify(addRequest),
      })
        .then((response) => response.json() as Promise<AddResponse>)
        .then((data) => {
          dispatch({
            type: "RECEIVE_ADD_PRODUCT",
            response: data,
          });
        }).catch((error) => {
            dispatch({
                type: "RECEIVE_ADD_PRODUCT",
                response: {error: error.message, success: false},
              })
          });

      dispatch({
        type: "REQUEST_ADD_PRODUCT",
        request: addRequest,
      });
    }
  },
};

const unloadedState: AddProductState = { isLoading: false };

export const reducer: Reducer<AddProductState> = (
  state: AddProductState | undefined,
  incomingAction: Action
): AddProductState => {
  if (state === undefined) {
    return unloadedState;
  }

  const action = incomingAction as KnownAction;
  switch (action.type) {
    case "REQUEST_ADD_PRODUCT":
      return {
        addRequest: state.addRequest,
        isLoading: true,
      };
    case "RECEIVE_ADD_PRODUCT":
      // Only accept the incoming data if it matches the most recent request. This ensures we correctly
      // handle out-of-order responses.
      return {
        addResponse: action.response,
        isLoading: false,
      };
  }
  return state;
};
