import { applyMiddleware, combineReducers, compose, createStore } from 'redux';
import thunk from 'redux-thunk';
import { connectRouter, routerMiddleware } from 'connected-react-router';
import { History } from 'history';
import { ApplicationState, reducers } from './';
import {loadUser} from 'redux-oidc';
import userManager from '../userManager';
import createOidcMiddleware from 'redux-oidc';

export default function configureStore(history: History, initialState?: ApplicationState) {
    userManager.events.addSilentRenewError(function (error: any) {
        console.error('error while renewing the access token', error);
    });
    const oidcMiddleware = createOidcMiddleware(userManager);
    const middleware = [
        thunk,
        routerMiddleware(history),
        oidcMiddleware
    ];

    const rootReducer = combineReducers({
        ...reducers,
        router: connectRouter(history)
    });

    const enhancers = [];
    const windowIfDefined = typeof window === 'undefined' ? null : window as any;
    if (windowIfDefined && windowIfDefined.__REDUX_DEVTOOLS_EXTENSION__) {
        enhancers.push(windowIfDefined.__REDUX_DEVTOOLS_EXTENSION__());
    }

    const store =  createStore(
        rootReducer,
        initialState,
        compose(applyMiddleware(...middleware), ...enhancers)
    );

    loadUser(store, userManager);
    return store;
}
