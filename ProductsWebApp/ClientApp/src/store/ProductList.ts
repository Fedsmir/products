import { Action, Reducer } from "redux";
import { AppThunkAction } from "./";

export interface ProductListState {
  isLoading: boolean;
  page?: number;
  pageSize?: number;
  productsResponse: ProductListResponse;
}

export interface ProductListResponse {
  products: Product[];
  count: number;
  success: boolean;
  error: string;
}

export interface Product {
  id: string;
  label: string;
  description: number;
  price: number;
}

export interface RequestProductListAction {
  type: "REQUEST_PRODUCTLIST";
  page: number;
  pageSize: number;
}

interface ReceiveProductListAction {
  type: "RECEIVE_PRODUCTLIST";
  response: ProductListResponse;
  page: number;
  pageSize: number;
}

export type KnownAction = RequestProductListAction | ReceiveProductListAction;

export const actionCreators = {
  requestProductList: (page: number, pageSize: number): AppThunkAction<KnownAction> => (
    dispatch,
    getState
  ) => {
    const appState = getState();
    if (appState && appState.productList && (page !== appState.productList.page || pageSize != appState.productList.pageSize)) {
      const headers = new Headers();
      headers.append("Accept", "application/json");
      headers.append("Content-Type", "application/json");
      fetch(`/Products?page=${page}&pagesize=${pageSize}`, {
        method: "GET",
        headers: headers
      })
        .then((response) => response.json() as Promise<ProductListResponse>)
        .then((data) => {
          dispatch({
            type: "RECEIVE_PRODUCTLIST",
            response: data,
            page: page,
            pageSize: pageSize 
          });
        }).catch((error) => {
          dispatch({
            type: "RECEIVE_PRODUCTLIST",
            response: { error: error.message, success: false, count: 0, products: []},
            page: page,
            pageSize: pageSize
          })
        });

      dispatch({
        type: "REQUEST_PRODUCTLIST",
        pageSize: pageSize,
        page: page
      });
    }
  },
};

const unloadedState: ProductListState = { isLoading: false, productsResponse: { error: '', success: true, count: 0, products: [] } };

export const reducer: Reducer<ProductListState> = (
  state: ProductListState | undefined,
  incomingAction: Action
): ProductListState => {
  if (state === undefined) {
    return unloadedState;
  }

  const action = incomingAction as KnownAction;
  switch (action.type) {
    case "REQUEST_PRODUCTLIST":
      return {
        productsResponse: state.productsResponse,
        page: action.page,
        pageSize: action.pageSize,
        isLoading: true,
      };
    case "RECEIVE_PRODUCTLIST":
      // Only accept the incoming data if it matches the most recent request. This ensures we correctly
      // handle out-of-order responses.
      return {
        productsResponse: action.response,
        isLoading: false,
        page: action.page,
        pageSize: action.pageSize,
      };
  }
  return state;
};
